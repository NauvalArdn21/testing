<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->get('/', 'Home::index');
$routes->get('/login', 'Auth::login');
$routes->post('/login/authenticate', 'Auth::authenticate', ['as' => 'login.authenticate']);
$routes->get('/logout', 'Auth::logout');

$routes->group('admin', ['filter' => 'auth'], function ($routes) {
    $routes->get('dashboard', 'Auth::dashboard');

    // Rute untuk pengguna (users)
    $routes->get('users', 'UserController::index', ['as' => 'user.index']);
    $routes->get('users/create', 'UserController::create', ['as' => 'user.create']);
    $routes->post('users/store', 'UserController::store', ['as' => 'user.store']);
    $routes->get('users/edit/(:segment)', 'UserController::edit/$1', ['as' => 'user.edit']);
    $routes->put('users/update/(:segment)', 'UserController::update/$1', ['as' => 'user.update']);
    $routes->delete('users/delete/(:segment)', 'UserController::delete/$1', ['as' => 'user.delete']);

    // Rute untuk pegawai (pegawai)
    $routes->get('pegawai', 'PegawaiController::index', ['as' => 'pegawai.index']);
    $routes->get('pegawai/create', 'PegawaiController::create', ['as' => 'pegawai.create']);
    $routes->post('pegawai/store', 'PegawaiController::store', ['as' => 'pegawai.store']);
    $routes->get('pegawai/edit/(:segment)', 'PegawaiController::edit/$1', ['as' => 'pegawai.edit']);
    $routes->put('pegawai/update/(:segment)', 'PegawaiController::update/$1', ['as' => 'pegawai.update']);
    $routes->delete('pegawai/delete/(:segment)', 'PegawaiController::delete/$1', ['as' => 'pegawai.delete']);

    $routes->get('/logout', 'Auth::logout', ['as' => 'logout']);

});
