<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\Controller;

class Auth extends Controller
{
    protected $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel(); // Load your user model
    }

    public function login()
    {
        // Logic to display the login form
        return view('auth/login'); // Adjust view file name and path as necessary
    }

    public function authenticate()
    {
        // Validate input
        $validationRules = [
            'username' => 'required',
            'password' => 'required'
        ];

        if (!$this->validate($validationRules)) {
            return redirect()->back()->withInput()->with('error', $this->validator->getErrors());
        }

        // Attempt to authenticate user
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        $user = $this->userModel->where('username', $username)->first();

        if (!$user || !password_verify($password, $user['password'])) {
            // Invalid credentials
            return redirect()->back()->withInput()->with('error', 'Invalid username or password.');
        }

        // Successful authentication
        $session = session();
        $userData = [
            'username' => $user['username'],
            'logged_in' => true
            // You can add more user data as needed
        ];
        $session->set($userData);

        return redirect()->to('/admin/dashboard'); // Redirect to dashboard upon successful login
    }

    public function logout()
    {
        // Logic to handle user logout
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }

    public function dashboard()
    {
        // Ensure user is authenticated
        $session = session();
        if (!$session->has('logged_in') || !$session->get('logged_in')) {
            return redirect()->to('/login');
        }

        // Load admin dashboard view
        return view('admin/dashboard'); // Adjust view file name and path as necessary
    }
}
