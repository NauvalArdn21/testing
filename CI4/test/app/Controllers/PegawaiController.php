<?php 

namespace App\Controllers;

use App\Models\PegawaiModel;
use CodeIgniter\Controller;

class PegawaiController extends Controller
{
    protected $model;

    public function __construct()
    {
        helper(['form', 'url']);
        $this->model = new PegawaiModel();
        session(); // Pastikan sesi dimulai
    }

    public function index()
    {
        $data['pegawai'] = $this->model->findAll();
        return view('admin/pegawai/index', $data);
    }

    public function create()
    {
        return view('admin/pegawai/create');
    }

    public function store()
    {
        if ($this->request->getMethod() !== 'post') {
            return redirect()->route('pegawai.index');
        }

        $validationRule = [  
            'foto' => [  
                'label' => 'Image File',  
                'rules' => 'uploaded[foto]'  
                    . '|is_image[foto]'  
                    . '|mime_in[foto,image/jpg,image/jpeg,image/gif,image/png,image/webp]'  
                    . '|max_size[foto,1000]'  
                    . '|max_dims[foto,4000,4000]',  
            ],  
        ];

        if ($this->validate($validationRule)) {
            $NIP = $this->request->getPost('NIP');
            $nama = $this->request->getPost('nama');
            $foto = $this->request->getFile('foto');
            $filename = $foto->getRandomName();
            $foto->move(ROOTPATH . 'public/uploads', $filename);

            $data = [
                'NIP' => $NIP,
                'nama' => $nama,
                'foto' => $filename,
            ];

            $save = $this->model->save($data);
            if ($save) {
                return redirect()->route('pegawai.index')
                ->with('success', 'Pegawai berhasil ditambahkan.');
            } else {
                session()->setFlashdata('error', $this->model->errors());
                return redirect()->back();
            }
        }

        session()->setFlashdata('error', $this->validator->getErrors());
        return redirect()->back()->withInput();
    }

    public function edit($NIP)
    {
        $data['pegawai'] = $this->model->find($NIP);
        return view('admin/pegawai/edit', $data);
    }

    public function update($NIP)
{
    if ($this->request->getMethod() !== 'put') {
        return redirect()->to('pegawai');
    }

    $validationRule = [
        'foto' => [
            'rules' => 'if_exist|max_size[foto,1000]|is_image[foto]|mime_in[foto,image/jpg,image/jpeg,image/gif,image/png,image/webp]',
            'label' => 'Image File'
        ]
    ];

    if ($this->validate($validationRule)) {
        $nama = $this->request->getPost('nama');
        $data = ['nama' => $nama];

        $foto = $this->request->getFile('foto');
        if ($foto && $foto->isValid() && !$foto->hasMoved()) {
            $filename = $foto->getRandomName();
            $foto->move(ROOTPATH . 'public/uploads', $filename);
            $data['foto'] = $filename;
        }

        $this->model->update($NIP, $data);
        return redirect()->to(base_url('pegawai'))->with('success', 'Pegawai berhasil diperbarui.');
    }

    session()->setFlashdata('error', $this->validator->getErrors());
    return redirect()->back()->withInput();
}


    public function delete($NIP)
    {
        $pegawai = $this->model->find($NIP);
        if ($pegawai) {
            $path = ROOTPATH . 'public/uploads/' . $pegawai['foto'];
            if (file_exists($path)) {
                unlink($path);
            }
            $this->model->delete($NIP);
        }
        return redirect()->to(base_url('pegawai'))->with('success', 'Pegawai berhasil dihapus.');
    }
}
?>
