<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class SessionInit implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        // Start session
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}
