<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'user';
    protected $primaryKey = 'id';

    protected $allowedFields = ['nama', 'username', 'password'];

    // Optional: Validation rules
    protected $validationRules = [
        'nama'     => 'required',
        'username' => 'required|is_unique[user.username]',
        'password' => 'required'
    ];

    // Optional: Get user by username
    public function getUserByUsername($username)
    {
        return $this->where('username', $username)->first();
    }
}
