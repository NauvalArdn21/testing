<?= $this->extend('layouts/stisla') ?>

<?= $this->section('title') ?>
Dashboard
<?= $this->endSection() ?>

<?= $this->section('page-title') ?>
Dashboard
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="section-body">
    <h2 class="section-title">Welcome to the Admin Dashboard</h2>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Admin Dashboard</h4>
                    <div class="card-header-action">
                        <a href="<?= route_to('logout') ?>" class="btn btn-danger">Logout</a>
                    </div>
                </div>
                <div class="card-body">
                    <p>Welcome, <?= session('username') ?>!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
