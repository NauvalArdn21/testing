<?= $this->extend('layouts/stisla') ?>

<?= $this->section('title') ?>
Tambah Pegawai
<?= $this->endSection() ?>

<?= $this->section('page-title') ?>
Tambah Pegawai
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Pegawai</h4>
                </div>
                <div class="card-body">
                    <?php if (session()->has('error')): ?>
                        <div class="alert alert-danger">
                            <?= session('error') ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= route_to('pegawai.store') ?>" method="post">                    
                    <?= csrf_field() ?>
                    <div class="form-group">
                        <label for="NIP">NIP</label>
                        <input type="text" name="NIP" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="foto">Foto</label>
                        <input type="file" name="foto" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="<?= route_to('pegawai.index') ?>" class="btn btn-secondary">Batal</a>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
