<?= $this->extend('layouts/stisla') ?>

<?= $this->section('title') ?>
Data pegawai
<?= $this->endSection() ?>

<?= $this->section('page-title') ?>
Data pegawai
<?= $this->endSection() ?>

<?= $this->section('style') ?>
<link rel="stylesheet" href="<?= base_url('library/datatables/media/css/jquery.dataTables.min.css') ?>">
<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4>Data pegawai</h4>
                        <div class="card-header-action">
                        <a href="<?= route_to('pegawai.create') ?>" class="btn btn-primary">
                            <i class="ion-ios-plus-outline"></i> Tambah pegawai
                        </a>
                    </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped w-100" id="pegawai-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Isi tabel dengan data pegawai -->
                                <?php foreach ($pegawai as $index => $pegawai): ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td><?= $pegawai['NIP'] ?></td>
                                        <td><?= $pegawai['nama'] ?></td>
                                        <td>
                                            <a href="<?= route_to('pegawai.edit', $pegawai['NIP']) ?>" class="btn btn-sm btn-warning">Edit</a>
                                            <a href="<?= route_to('pegawai.delete', $pegawai['NIP']) ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus pegawai ini?')">Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('library/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pegawai-table').DataTable();
    });
</script>
<?= $this->endSection() ?>
