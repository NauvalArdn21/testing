<?= $this->extend('layouts/stisla') ?>

<?= $this->section('title') ?>
Data User
<?= $this->endSection() ?>

<?= $this->section('page-title') ?>
Data User
<?= $this->endSection() ?>

<?= $this->section('style') ?>
<link rel="stylesheet" href="<?= base_url('library/datatables/media/css/jquery.dataTables.min.css') ?>">
<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4>Data User</h4>
                        <div class="card-header-action">
                        <a href="<?= route_to('user.create') ?>" class="btn btn-primary">
                            <i class="ion-ios-plus-outline"></i> Tambah User
                        </a>
                    </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped w-100" id="user-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Isi tabel dengan data user -->
                                <?php foreach ($user as $index => $user): ?>
                                    <tr>
                                        <td><?= $index + 1 ?></td>
                                        <td><?= $user['nama'] ?></td>
                                        <td><?= $user['username'] ?></td>
                                        <td>
                                            <a href="<?= route_to('user.edit', $user['id']) ?>" class="btn btn-sm btn-warning">Edit</a>
                                            <a href="javascript:void(0)" onclick="deleteUser(<?= $user['id'] ?>)" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('library/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#user-table').DataTable();
    });
    function deleteUser(id) {
    if (confirm('Apakah Anda yakin ingin menghapus user ini?')) {
        var form = document.createElement('form');
        form.method = 'POST';
        form.action = '<?= base_url("admin/users/delete") ?>/' + id;

        var csrfField = document.createElement('input');
        csrfField.type = 'hidden';
        csrfField.name = '<?= csrf_token() ?>';
        csrfField.value = '<?= csrf_hash() ?>';
        form.appendChild(csrfField);

        var hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = '_method';
        hiddenField.value = 'DELETE';
        form.appendChild(hiddenField);

        document.body.appendChild(form);
        form.submit();
    }
}
</script>
<?= $this->endSection() ?>
