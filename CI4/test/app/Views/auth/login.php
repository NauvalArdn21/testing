<?= $this->extend('layouts/login') ?>

<?= $this->section('content') ?>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card shadow-lg">
                <div class="card-header text-center bg-primary text-white">
                    <h4>Login</h4>
                </div>
                <div class="card-body p-4">
                    <form action="<?= route_to('login.authenticate') ?>" method="post">
                        <?= csrf_field() ?>
                        <div class="mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input type="text" class="form-control" id="username" name="username" required>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                        <div class="mb-3 d-flex justify-content-center">
                            <?php if (session()->getFlashdata('error')): ?>
                                <div class="alert alert-danger text-center">
                                    <?= session()->getFlashdata('error') ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
body {
    background: #f5f5f5;
    font-family: 'Arial', sans-serif;
}

.card {
    border-radius: 10px;
}

.card-header {
    background: #007bff;
    border-bottom: 0;
}

.card-body {
    padding: 2rem;
}

.btn-primary {
    background: #007bff;
    border: none;
}

.btn-primary:hover {
    background: #0056b3;
}

.form-label {
    font-weight: bold;
    color: #007bff;
}

.form-control {
    border-radius: 5px;
}

.container {
    margin-top: 5%;
}
</style>
<?= $this->endSection() ?>
