<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title><?= $this->renderSection('title') ?> &mdash; SarahVi</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?= base_url('library/bootstrap/dist/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- CSS Libraries -->
    <?= $this->renderSection('style') ?>

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?= base_url('css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('css/components.css') ?>">

    <!-- Start GA -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>
    <!-- END GA -->
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <!-- Header -->
            <?= $this->include('layouts/stisla/navbar') ?>

            <!-- Sidebar -->
            <?= $this->include('layouts/stisla/sidebar') ?>

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1><?= $this->renderSection('page-title') ?></h1>
                    </div>

                    <div class="section-body">
                        <?= $this->renderSection('content') ?>
                    </div>
                </section>
            </div>

            <!-- Footer -->
            <?= $this->include('layouts/stisla/footer') ?>
        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="<?= base_url('library/jquery/dist/jquery.min.js') ?>"></script>
    <script src="<?= base_url('library/popper.js/dist/umd/popper.js') ?>"></script>
    <script src="<?= base_url('library/tooltip.js/dist/umd/tooltip.js') ?>"></script>
    <script src="<?= base_url('library/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('library/jquery.nicescroll/dist/jquery.nicescroll.min.js') ?>"></script>
    <script src="<?= base_url('library/moment/min/moment.min.js') ?>"></script>
    <script src="<?= base_url('js/stisla.js') ?>"></script>
    
    <?= $this->renderSection('scripts') ?>

    <!-- Template JS File -->
    <script src="<?= base_url('js/scripts.js') ?>"></script>
    <script src="<?= base_url('js/custom.js') ?>"></script>
</body>

</html>
