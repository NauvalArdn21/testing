<!-- app/Views/layouts/stisla/sidebar.php -->
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">Admin Panel</a>
        </div>
        
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown">
                <a href="<?= site_url('admin/dashboard') ?>" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Users</li>
            <li class="nav-item dropdown">
                <a href="<?= site_url('admin/users') ?>" class="nav-link"><i class="fas fa-users"></i><span>Users</span></a>
                <a href="<?= site_url('admin/pegawai') ?>" class="nav-link"><i class="fa-solid fa-user-tie"></i><span>Pegawai</span></a>
            </li>
        </ul>
    </aside>
</div>
