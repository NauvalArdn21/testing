import { Sequelize } from "sequelize";

const db = new Sequelize('testing', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

export default db;