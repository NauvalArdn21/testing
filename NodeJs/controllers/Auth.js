import User from "../models/UserModel.js";
import argon2 from "argon2";

export const login = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        username: req.body.username,
      },
    });

    if (!user) return res.status(404).json({ msg: "User tidak ditemukan" });

    const match = await argon2.verify(user.password, req.body.password);
    if (!match) return res.status(400).json({ msg: "Wrong Password" });

    req.session.userId = user.id;
    const { name, username } = user;

    res.status(200).json({name, username });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};


export const logout = (req, res) => {
  req.session.destroy((err) => {
    if (err) return res.status(400).json({ msg: "Tidak dapat logout" });
    res.status(200).json({ msg: "Anda telah logout" });
  });
};
