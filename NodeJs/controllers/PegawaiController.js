import Pegawai from "../models/PegawaiModel.js";

export const createPegawai = async (req, res) => {
  const { NIP, nama, foto } = req.body;

  try {
    // Simpan foto ke direktori src/foto dengan nama yang unik

    const pegawai = await Pegawai.create({
      NIP,
      nama,
      foto // Simpan path foto di database
    });

    res.status(201).json({ message: 'Pegawai berhasil ditambahkan', data: pegawai });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

export const updatePegawai = async (req, res) => {
    const { NIP } = req.params;
    const { nama, foto } = req.body;
  
    try {
      let pegawai = await Pegawai.findOne({
        where: { NIP }
      });
  
      if (!pegawai) {
        return res.status(404).json({ message: 'Pegawai tidak ditemukan' });
      }
  
      pegawai.nama = nama;
      pegawai.foto = foto;
  
      await pegawai.save();
  
      res.status(200).json({ message: 'Pegawai berhasil diperbarui', data: pegawai });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  };

  export const deletePegawai = async (req, res) => {
  const { NIP } = req.params;

  try {
    const pegawai = await Pegawai.findOne({
      where: { NIP }
    });

    if (!pegawai) {
      return res.status(404).json({ message: 'Pegawai tidak ditemukan' });
    }

    await pegawai.destroy();

    res.status(200).json({ message: 'Pegawai berhasil dihapus' });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

