import express from 'express';
import cors from 'cors';
import session from 'express-session';
import SequelizeStore from 'connect-session-sequelize';
import db from './config/Database.js';
import dotenv from 'dotenv';
import UserRoute from './routes/UserRoute.js';
import AuthRoute from './routes/AuthRoute.js';
import PegawaiRoute from './routes/PegawaiRoute.js';

dotenv.config();

const app = express();
const PORT = process.env.APP_PORT || 5000;

const SequelizeStoreInstance = SequelizeStore(session.Store);

const sessionStore = new SequelizeStoreInstance({
  db: db,
  expiration: 24 * 60 * 60 * 1000, // Durasi sesi dalam milidetik (misalnya 24 jam)
});

app.use(
  session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
    cookie: {
      secure: 'auto', // 'auto' untuk HTTPS
    },
  })
);

app.use(
  cors({
    credentials: true,
    origin: 'http://localhost:3000', // Ganti dengan URL aplikasi frontend Anda
  })
);

app.use(express.json());
app.use(UserRoute);
app.use(AuthRoute);
app.use(PegawaiRoute);

// Serve static files from 'src' directory
app.use(express.static('src'));

// Sync SequelizeStore with database
sessionStore.sync();

app.listen(PORT, () => {
  console.log(`Server up and running on port ${PORT}`);
});
