import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const Pegawai = db.define('pegawai', {
    NIP: {
        type: DataTypes.STRING(18), // Menentukan panjang maksimal 18 karakter
        primaryKey: true,
        allowNull: false,
        validate: {
          notEmpty: true,
        }
      },
    nama: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },
    foto: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },
   
  }, {
    freezeTableName: true,
timestamps:false,
  });

  export default Pegawai;



