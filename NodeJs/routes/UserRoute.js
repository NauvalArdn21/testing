import express from "express";
import { 
    getUsers,
    createUser,
    updateUser,
    deleteUser

 } from "../controllers/UserController.js";
 import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/users', getUsers);
router.post('/users', createUser);
router.patch('/users/:id', verifyUser, updateUser);
router.delete('/users/:id',deleteUser);


export default router;