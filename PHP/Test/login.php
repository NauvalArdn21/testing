<?php
session_start();
$servername = "localhost";
$username = "root"; // sesuaikan dengan username database Anda
$password = ""; // sesuaikan dengan password database Anda
$dbname = "testing"; // sesuaikan dengan nama database Anda

// Membuat koneksi ke database
$conn = new mysqli($servername, $username, $password, $dbname);

// Memeriksa koneksi
if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $user = $_POST['username'];
    $pass = $_POST['password'];

    // Melakukan query untuk memeriksa username
    $sql = "SELECT * FROM user WHERE username = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $user);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        // Verifikasi password hash
        if (password_verify($pass, $row['password'])) {
            // Login berhasil
            $_SESSION['username'] = $row['username'];
            $_SESSION['nama'] = $row['nama'];
            header("Location: dashboard.php");
        } else {
            // Login gagal
            echo "Password salah";
        }
    } else {
        // Login gagal
        echo "Username tidak ditemukan";
    }

    $stmt->close();
}

$conn->close();
?>
